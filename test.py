Introduction to Linear Algebra for Deep Learning
In this E-tivity, you will practice some basic linear algebra operations that were introduced during the Online Lab. The purpose of this lab is mainly to improve your familiarity with the Python language and to the basic linear algebra concepts that are used in deep learning.

Load the Numpy library

import numpy
import numpy as np
Vectors
Create vectors v of length 12 containing random floats between -2 and 6, and w of length 12 containing random floats between 10 and 22
Print the length of each vector using two different function calls.
Compute the sum of the two vectors
Compute the dot product of these two vectors; v⋅wv⋅w
Compute the cross product of these two randomly generated vectors a×ba×b, where aa and bb are both of length 3.

v = np.arange(12) - 12   
w = np.arange(12) + 12   

v
print(v)
print(w)
[-12 -11 -10  -9  -8  -7  -6  -5  -4  -3  -2  -1]
[12 13 14 15 16 17 18 19 20 21 22 23]

# Sum of v and w
# Insert Here

# Dot prod of v and w
# Insert Here

a = # Insert Here
b = # Insert Here
​
print("a = ",# Insert Here)
print("b = ", # Insert Here)
      
# Compute cross product of a and b      
print(# Insert Here)
Matrices
Create three randomly generated matrics A, B and C with dimensions 12x5, 12x5 and 5x8 respectively, with integer values between -10 and 10
Compute the sum of A and B
Compute the product of A and C (A⋅CA⋅C)
Print the shape of AA, BB and A⋅BA⋅B. Are these dimensions expected? Try writting out the maths for a 3x4 and a 4x2 matrix to get a better intuition as to what is happening
Compute the transpose of the matrix A
Compute the product of B and the transpose of A (B⋅ATB⋅AT)
Print the shape of the resulting matrix of B⋅ATB⋅AT. What is this type of matrix called?
Generate another matrix FF with the same dimensions as B⋅ATB⋅AT, filled with random integers between -10 and -20.
Compute the products F⋅(B⋅AT)F⋅(B⋅AT) and (B⋅AT)⋅F(B⋅AT)⋅F. Are these matrices identical? Is this result expected?

A = # Insert Here
B = # Insert Here
C = # Insert Here

# Sum of A and B
# Insert Here

# Dot product of A and C
D = # Insert Here
​
print(D)

# Output shapes of matrices
print(# Insert Here)
print(# Insert Here)
print(# Insert Here)

# Transpose of A
np.transpose(# Insert Here)

# Dot prod of B and A^T
E = # Insert Here
​
# print E
# Insert Here

# Output shape of E
# Insert Here

# Generate F
F = # Insert Here

# Dot prod of F and E
print(# Insert Here)
# Print its shape
print(# Insert Here)
    
# Dot prod of E and F
print(# Insert Here)
# Print its shape
print(# Insert Here)
​
LU Factorisation with Partial Pivoting
Consider the matrix A and the vector b below. We wish to solve the system of equations defined by Ax=bAx=b (ie. solve for vector x). To do this, we decompose the matrix A into three matrices; P, L and U, where P is the permutation matrix, L is a lower triangular matrix with all 1's on the digonal, and U is an upper triangular matrix. Your task is to

Conduct LU factorisation with partial pivoting on the matrix A
Use the generated matrices P, L and U to soleve the system Ax=bAx=b.
Verify your solution for xx using the solve function from scipy.linalg.

from scipy.linalg import lu, inv
​
A = np.array([[9, 3, 2, 0, 7], 
              [7, 6, 9, 6, 4],
              [2, 7, 7, 8, 2],
              [0, 9, 7, 2, 2],
              [7,3,6,4,3]])
​
b = np.array([35,58,53,37,39])
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-2-5518a438856c> in <module>()
      1 from scipy.linalg import lu, inv
      2 
----> 3 A = np.array([[9, 3, 2, 0, 7], 
      4               [7, 6, 9, 6, 4],
      5               [2, 7, 7, 8, 2],

NameError: name 'np' is not defined


# Decompose A into P,L and U
# Insert Here
​
# Solve for X
x = # Insert Here

from scipy.linalg import solve
​
# Solve for X using SciPy Library
x_ = # Insert Here
​
# If matrices are the same or not, generate appropriate output
if(# Insert Here):
    # Insert Here
else:
    # Insert Here
Representing a Dataset as a Tensor
You will often hear the word tensor being used when learning about neural networks. The way you can think of a tensor is an n-dimensional matrix (representing n-rank tensor). For a tensor of rank 2 (similar to a two dimensional matrix), if each row of the tensor represents a single sample of our data, then each column represents a different feature. In neural networks, the tensor's elements can be of a different datatype (think of each column being able to be a different datatype).

To get more familiar with the way we will be handling data, we are going to generate a tensor of rank 2 where each row represents a sample and each column a feature.

First we will define a small group of data in seperate arrays based on their features for fruit.


label = np.array(["Orange", "Pear", "Apple", "Bananna", "Mango", "Grape", "Melon", "Lemon" , "Strawberry"])
weight = np.array([1.2,1.7,0.9,2.0,3.2,0.3,4.6,0.8,0.4])
colour = np.array(["orange","green","red","yellow","green","green","yellow","yellow","red"])
origin = np.array(["Country 1","Country 2","Country 2","Country 3","Country 3","Country 1","Country 2",
                  "Country 1","Country 2"])
age_since_harvest_weeks = np.array([3,6,12,1,8,2,3,15,1])
For you to do:

Generate a rank 2 tensor containing the entire dataset. The tensor can be represented as a matrix in Python, however, you must ensure that the datatypes of each feature is correct (int as an integer, string as a string, etc). Note: It is common notation to have the final column of a dataset representing the outcome (ie the label).

# Define the datatype
# Insert Here
​
​
# Generate a matrix/tensor representing each feature and sample
fruit = # Insert Here
​
​
# Set each column of the tensor
# Insert Here
.
.
.
# Insert Here
​
​
# Output tensor
print(# Insert Here)
Note that you will probably never need to create a datastructure manually like this, however it is useful to know how it is constructed to gain a better understanding of it and structured arrays in general.

Split the array into a 'train' and 'test' set using slicing. The training set should have 70% of the samples and the testing set the remainder.